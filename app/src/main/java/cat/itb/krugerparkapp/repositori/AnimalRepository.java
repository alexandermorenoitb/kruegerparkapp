package cat.itb.krugerparkapp.repositori;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;

import java.util.List;


import cat.itb.krugerparkapp.model.Animal;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AnimalRepository {

    private LiveData<List<DatabaseReference>> animals;
    FirebaseDatabase database;
    DatabaseReference myRef;
    //private Uri mImageUri;

    public AnimalRepository(Context context){
        
        /*
         Objecte per accedir a firebase/ firestore
         */
        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("animals");

    }

    public LiveData<List<DatabaseReference>> getAnimal(){return animals;}

    public LiveData<List<DatabaseReference>> getAnimals() {
        //accés a dades firestore

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        return animals;
    }
}