package cat.itb.krugerparkapp.ui.main;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import cat.itb.krugerparkapp.model.Animal;

import cat.itb.krugerparkapp.model.FirebaseQueryLiveData;
import cat.itb.krugerparkapp.repositori.AnimalRepository;


public class MainViewModel extends AndroidViewModel {
    private AnimalRepository animalRepository;
    private LiveData<List<Animal>> animals;
    private boolean isUpdate =false;
    private static final DatabaseReference ANIMALS_REF =
            FirebaseDatabase.getInstance().getReference("/animals");

    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(ANIMALS_REF);

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

//    public LiveData <List<Animal>> getAnimals(){
//        return animalRepository.getAnimals();
//    }

    @NonNull
    public LiveData<DataSnapshot> getDataSnapshotLiveData() {
        return liveData;
    }

}
