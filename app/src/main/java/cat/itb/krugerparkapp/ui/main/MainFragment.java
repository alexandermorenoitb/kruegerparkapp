package cat.itb.krugerparkapp.ui.main;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;

import cat.itb.krugerparkapp.R;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    RecyclerView recyclerView;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated( view, savedInstanceState );
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        MaterialButton btnLookMap = getView().findViewById(R.id.btn_lookMap);
        btnLookMap.setOnClickListener(this::changeToMapView);

        MaterialButton btnAnimalsList = getView().findViewById(R.id.btn_lookAnimalsList);
        btnAnimalsList.setOnClickListener(this::changeToAnimalsList);

        MaterialButton btnWhoIsThatAnimal = getView().findViewById(R.id.btn_whosIsThatAnimal);
        btnWhoIsThatAnimal.setOnClickListener(this::changeToWhoIsThatAnimal);
    }

    private void changeToWhoIsThatAnimal(View view) {
        Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_whoIsThatAnimal);
    }


    private void changeToMapView(View view) {
        Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_mapsActivity);
    }

    private void changeToAnimalsList(View view) {
        Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_listFragment);
    }

}


