package cat.itb.krugerparkapp.ui.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import cat.itb.krugerparkapp.R;

public class MapsDialog extends AppCompatDialogFragment {

    private EditText animalName;
    private MapsDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.maps_dialog,null);

        builder.setView(view)
                .setTitle("Upload")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Upload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String nombreAnimal = animalName.getText().toString();
                        listener.applyText(nombreAnimal);
                    }
                });

        animalName = view.findViewById(R.id.editText_animalName);

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (MapsDialogListener) context;
        } catch (ClassCastException e) {
            throw  new ClassCastException(context.toString()+"debe de implementar MapsDialogListener");
        }
    }

    public interface MapsDialogListener{
        void applyText(String nombreAnimal);
    }

}
