package cat.itb.krugerparkapp.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import cat.itb.krugerparkapp.R;

import static android.os.Environment.getExternalStoragePublicDirectory;
import static android.os.Environment.getExternalStorageState;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, MapsDialog.MapsDialogListener {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private final int PICK_IMAGE_REQUEST = 22;
    private GoogleMap mMap;
    public int cont;
    FloatingActionButton openCameraBtn;
    FloatingActionButton uploadImage;
    FloatingActionButton mapsView;
    FloatingActionButton locationBtn;
    ImageView imageView;
    Button btnTakePic;
    String mCurrentPhotoPath;
    File photoFile;

    //    MaterialButton openCameraBtn;
    private Uri filePath;

    FirebaseStorage storage;
    StorageReference storageReference;

    //private DocumentReference mDocRef = FirebaseFirestore.getInstance().document("Animal/images");

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        //googleMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION }, 1222);
        }
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // get the Firebase  storage reference
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        cont = 0;

        //buttons
        openCameraBtn = findViewById(R.id.btn_Add);
//        openCameraBtn.setOnClickListener(this::dispatchTakePictureIntent);
        openCameraBtn.setOnClickListener(this::openDialog);

        mapsView = findViewById(R.id.btn_mapsViews);
        mapsView.setOnClickListener(this::cambiarVistaMapa);

        locationBtn = findViewById(R.id.btn_location);
        locationBtn.setOnClickListener(this::actualizarLocalizacion);

        LayoutInflater inflater = this.getLayoutInflater();
        View DialogView = inflater.inflate(R.layout.maps_dialog,null);

        imageView=DialogView.findViewById(R.id.imageToUpload);

        btnTakePic=DialogView.findViewById(R.id.btn_takePic);
        btnTakePic.setOnClickListener(this::dispatchTakePictureIntent);

        uploadImage = DialogView.findViewById(R.id.btn_UploadImg);
        uploadImage.setOnClickListener(this::selectImage);


    }

    private void openDialog(View view){
        MapsDialog mapsDialog = new MapsDialog();
        mapsDialog.show(getSupportFragmentManager(),"Maps Dialog");
    }

    private void actualizarLocalizacion(View view) {

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;
        // Or use LocationManager.GPS_PROVIDER
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION }, 1222);
        }
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
        LatLng ubicacion = new LatLng(lastKnownLocation.getLatitude(),lastKnownLocation.getLongitude());

        mMap.addMarker(new MarkerOptions().position(ubicacion).title("Marker in Anywhere"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ubicacion));

    }

    private void cambiarVistaMapa(View view){
        if(cont==0){
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            Toast.makeText(getBaseContext(),"Vista Satélite",Toast.LENGTH_SHORT).show();
            cont++;
        }else if(cont==1){
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            Toast.makeText(getBaseContext(),"Vista Híbrida",Toast.LENGTH_SHORT).show();
            cont++;
        }else{
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            Toast.makeText(getBaseContext(),"Vista Normal",Toast.LENGTH_SHORT).show();
            cont=0;
        }
    }


    public void dispatchTakePictureIntent(View view) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intentParent = getIntent();
        setResult(RESULT_OK, intentParent);

        Intent takePictureIntent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE);
        mCurrentPhotoPath="file:///sdcard/photo.jpg";
        Uri uri  = Uri.parse(mCurrentPhotoPath);
        takePictureIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
        if (takePictureIntent.resolveActivity( getPackageManager() ) != null) {
            startActivityForResult( takePictureIntent, REQUEST_IMAGE_CAPTURE );
        }

    }

    private void createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        mCurrentPhotoPath="/storage/emulated/0/Kruger";
        File storageDir = getExternalFilesDir(mCurrentPhotoPath);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Toast.makeText(this, mCurrentPhotoPath, Toast.LENGTH_LONG).show();

    }

    // Select Image method
    public void selectImage(View view)
    {

        // Defining Implicit Intent to mobile gallery
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(
                        intent,
                        "Select Image from here..."),
                PICK_IMAGE_REQUEST);
    }

    // Override onActivityResult method
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent data)
    {

        super.onActivityResult(requestCode,
                resultCode,
                data);

        // checking request code and result code
        // if request code is PICK_IMAGE_REQUEST and
        // resultCode is RESULT_OK
        // then set image in the image view
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
            && data != null
            && data.getData() != null) {

                filePath = data.getData();
                // Let's read picked image path using content resolver
                String[] BitmapfilePath = { MediaStore.Images.Media.DATA };
                Cursor cursor = getContentResolver().query(filePath, BitmapfilePath, null, null, null);
                cursor.moveToFirst();
                String imagePath = cursor.getString(cursor.getColumnIndex(BitmapfilePath[0]));

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

                // Do something with the bitmap
                imageView.setImageBitmap(bitmap);
                imageView.setVisibility(View.VISIBLE);

                // At the end remember to close the cursor or you will end with the RuntimeException!
                cursor.close();

//                uploadImage();
            }
            else if (requestCode == REQUEST_IMAGE_CAPTURE) {
                filePath = Uri.fromFile(new File(getExternalStorageState()));
                Bitmap bitmap;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filePath);
                    imageView.setImageBitmap(bitmap);
                } catch (IOException e) {
                    System.out.println("*** ERRORRR *** -> -> "+e.getLocalizedMessage());
                }

//            Bitmap photo = (Bitmap) data.getExtras().get("data");


        }

    }

    private void uploadImage(String animalName)
    {
        if (filePath != null) {

            // Code for showing progressDialog while uploading
            ProgressDialog progressDialog
                    = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            // Defining the child of storageReference
            StorageReference ref
                    = storageReference
                    .child(
                            "images/"
                                    +animalName+"/"
                                    + UUID.randomUUID().toString());

            // adding listeners on upload
            // or failure of image
            ref.putFile(filePath)
                    .addOnSuccessListener(
                            new OnSuccessListener<UploadTask.TaskSnapshot>() {

                                @Override
                                public void onSuccess(
                                        UploadTask.TaskSnapshot taskSnapshot)
                                {

                                    // Image uploaded successfully
                                    // Dismiss dialog
                                    progressDialog.dismiss();
                                    Toast
                                            .makeText(getBaseContext(),
                                                    "Image Uploaded!!",
                                                    Toast.LENGTH_SHORT)
                                            .show();
                                }
                            })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e)
                        {

                            // Error, Image not uploaded
                            progressDialog.dismiss();
                            Toast
                                    .makeText(getBaseContext(),
                                            "Failed " + e.getMessage(),
                                            Toast.LENGTH_SHORT)
                                    .show();
                        }
                    })
                    .addOnProgressListener(
                            new OnProgressListener<UploadTask.TaskSnapshot>() {

                                // Progress Listener for loading
                                // percentage on the dialog box
                                @Override
                                public void onProgress(
                                        UploadTask.TaskSnapshot taskSnapshot)
                                {
                                    double progress
                                            = (100.0
                                            * taskSnapshot.getBytesTransferred()
                                            / taskSnapshot.getTotalByteCount());
                                    progressDialog.setMessage(
                                            "Uploaded "
                                                    + (int)progress + "%");
                                }
                            });
        }
    }

    /**
     * Method from MapsDialog class, when ok bottom is pushed
     * */
    @Override
    public void applyText(String nombreAnimal) {
        uploadImage(nombreAnimal);
    }


}
