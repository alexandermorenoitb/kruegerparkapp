package cat.itb.krugerparkapp.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.util.List;
import java.util.Locale;

import cat.itb.krugerparkapp.R;
import cat.itb.krugerparkapp.adapter.AnimalAdapter;
import cat.itb.krugerparkapp.model.Animal;


public class ListFragment extends Fragment {

    private MainViewModel mViewModel;
    AnimalAdapter adapter;
    FirebaseDatabase database;
    DatabaseReference myRef;
    TextView animalName;
    //TextView tvPrice;
    FirebaseStorage storage;
    StorageReference storageRef;


    RecyclerView recyclerView;

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated( view, savedInstanceState );
        //recyclerView=view.findViewById(R.id.recycleViewList);
        animalName = view.findViewById(R.id.animalNameRow);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();

// Get reference to the file
        StorageReference forestRef = storageRef.child("images/e611afa2-e0f3-4283-b502-1103d9bb0cd9");

//        forestRef.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
//            @Override
//            public void onSuccess(StorageMetadata storageMetadata) {
//                // Metadata now contains the metadata for 'images/forest.jpg'
//                System.out.println("** METADATOS** - " + storageMetadata.getName() + "***");
//                //animalName.setText(storageMetadata.getName());
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception exception) {
//                // Uh-oh, an error occurred!
//                Toast.makeText(getContext(),"ERROR!!"+exception.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
//            }
//        });

//        recyclerView.setHasFixedSize(true);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
//        recyclerView.setLayoutManager(layoutManager);
//
//        adapter = new AnimalAdapter();
//        recyclerView.setAdapter(adapter);
//
//       LiveData<List<Animal>> animals = mViewModel.getAnimals();
//       animals.observe(getViewLifecycleOwner(), this::onAnimalChanged);

//       tvPrice = findViewById(R.id.price);
//        LiveData<DataSnapshot> liveData = mViewModel.getDataSnapshotLiveData();
//
//        liveData.observe(this, new Observer<DataSnapshot>() {
//            @Override
//            public void onChanged(@Nullable DataSnapshot dataSnapshot) {
//                if (dataSnapshot != null) {
//                    // update the UI here with values in the snapshot
//                    int animal = dataSnapshot.child("Views").getValue(Integer.class);
//                    animalName.setText(String.format(Locale.getDefault(), "%d", animal));
////                    Float price = dataSnapshot.child("price").getValue(Float.class);
////                    tvPrice.setText(String.format(Locale.getDefault(), "%.2f", price));
//                }
//            }
//        });

    }

    private void onAnimalChanged(List<Animal> animals) {
        adapter.setAnimalList(animals);
    }

}
