package cat.itb.krugerparkapp.ui.main;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

import cat.itb.krugerparkapp.R;


public class WhoIsThatAnimal extends Fragment {


    private MainViewModel mViewModel;
    private Boolean boolLion = false;
    private Boolean boolZebra = false;
    private Boolean boolElephant = false;
    private Boolean boolSnake = false;
    private Boolean boolHyena = false;
    private Boolean boolGiraffe = false;
    private Boolean boolMonkey = false;
    private Boolean boolRhyno = false;
    int randomNumber;
    MediaPlayer mediaPlayer = new MediaPlayer();

    ImageView imageLion;
    ImageView imageZebra;
    ImageView imageElephant;
    ImageView imageSnake;
    ImageView imageHyena;
    ImageView imageGiraffe;
    ImageView imageMonkey;
    ImageView imageRhyno;

    int count = 0;

    private int randomMethod(){
        randomNumber = (int) (Math.random() * 6);
        return randomNumber;
    }

    public static WhoIsThatAnimal newInstance() {
        return new WhoIsThatAnimal();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_who_is_that_animal, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        imageLion = getView().findViewById(R.id.imageLion);
        imageZebra = getView().findViewById(R.id.imageZebra);
        imageElephant = getView().findViewById(R.id.imageElephant);
        imageSnake = getView().findViewById(R.id.imageSnake);
        imageHyena = getView().findViewById(R.id.imageHyena);
        imageGiraffe = getView().findViewById(R.id.imageGiraffe);
        imageRhyno = getView().findViewById(R.id.imageRhyno);
        imageMonkey = getView().findViewById(R.id.imageMono);

        startGame();

    }
    private void startGame() {
        randomNumber = randomMethod();

        count++;

        if (randomMethod() == 0) {

            mediaPlayer = MediaPlayer.create(getContext(), R.raw.serpiente_sonido);
            mediaPlayer.start();


            imageElephant.setOnClickListener(this::toastIncorrecte);
            imageGiraffe.setOnClickListener(this::toastIncorrecte);
            imageHyena.setOnClickListener(this::toastIncorrecte);
            imageLion.setOnClickListener(this::toastIncorrecte);
            imageMonkey.setOnClickListener(this::toastIncorrecte);
            imageRhyno.setOnClickListener(this::toastIncorrecte);
            imageZebra.setOnClickListener(this::toastIncorrecte);
            imageSnake.setOnClickListener(this::methodToast);


        }
        if (randomNumber == 1) {
            mediaPlayer = MediaPlayer.create(getContext(), R.raw.leon_sonido);
            mediaPlayer.start();

            imageElephant.setOnClickListener(this::toastIncorrecte);
            imageGiraffe.setOnClickListener(this::toastIncorrecte);
            imageHyena.setOnClickListener(this::toastIncorrecte);
            imageSnake.setOnClickListener(this::toastIncorrecte);
            imageMonkey.setOnClickListener(this::toastIncorrecte);
            imageRhyno.setOnClickListener(this::toastIncorrecte);
            imageZebra.setOnClickListener(this::toastIncorrecte);
            imageLion.setOnClickListener(this::methodToast);

        }
        if (randomNumber == 2) {
            mediaPlayer = MediaPlayer.create(getContext(), R.raw.hyena);
            mediaPlayer.start();

            imageElephant.setOnClickListener(this::toastIncorrecte);
            imageGiraffe.setOnClickListener(this::toastIncorrecte);
            imageLion.setOnClickListener(this::toastIncorrecte);
            imageMonkey.setOnClickListener(this::toastIncorrecte);
            imageRhyno.setOnClickListener(this::toastIncorrecte);
            imageZebra.setOnClickListener(this::toastIncorrecte);
            imageSnake.setOnClickListener(this::toastIncorrecte);
            imageHyena.setOnClickListener(this::methodToast);

        }
        if (randomNumber == 3) {
            mediaPlayer = MediaPlayer.create(getContext(), R.raw.elephant);
            mediaPlayer.start();


            imageGiraffe.setOnClickListener(this::toastIncorrecte);
            imageHyena.setOnClickListener(this::toastIncorrecte);
            imageLion.setOnClickListener(this::toastIncorrecte);
            imageMonkey.setOnClickListener(this::toastIncorrecte);
            imageRhyno.setOnClickListener(this::toastIncorrecte);
            imageZebra.setOnClickListener(this::toastIncorrecte);
            imageSnake.setOnClickListener(this::toastIncorrecte);
            imageElephant.setOnClickListener(this::methodToast);
        }

        if (randomNumber == 4) {
            mediaPlayer = MediaPlayer.create(getContext(), R.raw.donkey);
            mediaPlayer.start();

            imageElephant.setOnClickListener(this::toastIncorrecte);
            imageHyena.setOnClickListener(this::toastIncorrecte);
            imageLion.setOnClickListener(this::toastIncorrecte);
            imageMonkey.setOnClickListener(this::toastIncorrecte);
            imageRhyno.setOnClickListener(this::toastIncorrecte);
            imageZebra.setOnClickListener(this::toastIncorrecte);
            imageSnake.setOnClickListener(this::toastIncorrecte);
            imageGiraffe.setOnClickListener(this::methodToast);
        }

        if (randomNumber == 5) {
            mediaPlayer = MediaPlayer.create(getContext(), R.raw.monkey);
            mediaPlayer.start();

            imageElephant.setOnClickListener(this::toastIncorrecte);
            imageGiraffe.setOnClickListener(this::toastIncorrecte);
            imageHyena.setOnClickListener(this::toastIncorrecte);
            imageLion.setOnClickListener(this::toastIncorrecte);
            imageRhyno.setOnClickListener(this::toastIncorrecte);
            imageZebra.setOnClickListener(this::toastIncorrecte);
            imageSnake.setOnClickListener(this::toastIncorrecte);
            imageMonkey.setOnClickListener(this::methodToast);
        }
        if (randomNumber == 6) {
            mediaPlayer = MediaPlayer.create(getContext(), R.raw.zebra);
            mediaPlayer.start();

            imageElephant.setOnClickListener(this::toastIncorrecte);
            imageGiraffe.setOnClickListener(this::toastIncorrecte);
            imageHyena.setOnClickListener(this::toastIncorrecte);
            imageLion.setOnClickListener(this::toastIncorrecte);
            imageMonkey.setOnClickListener(this::toastIncorrecte);
            imageRhyno.setOnClickListener(this::toastIncorrecte);
            imageSnake.setOnClickListener(this::toastIncorrecte);
            imageZebra.setOnClickListener(this::methodToast);
        }
        if (randomNumber == 7) {
            mediaPlayer = MediaPlayer.create(getContext(), R.raw.rhino);
            mediaPlayer.start();

            imageElephant.setOnClickListener(this::toastIncorrecte);
            imageGiraffe.setOnClickListener(this::toastIncorrecte);
            imageHyena.setOnClickListener(this::toastIncorrecte);
            imageLion.setOnClickListener(this::toastIncorrecte);
            imageMonkey.setOnClickListener(this::toastIncorrecte);
            imageZebra.setOnClickListener(this::toastIncorrecte);
            imageSnake.setOnClickListener(this::toastIncorrecte);
            imageRhyno.setOnClickListener(this::methodToast);
        }
    }

    private void methodToast(View view) {
        Toast.makeText(getActivity(), "CORRECT!", Toast.LENGTH_LONG).show();
        mediaPlayer.stop();
        try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (count<=8){
            startGame();
        }else{
            Toast.makeText(getActivity(), "GAME FINISHED!", Toast.LENGTH_LONG).show();
        }
    }

    private void toastIncorrecte(View view){
        Toast.makeText(getActivity(),"YOU FAIL!",Toast.LENGTH_LONG).show();
    }

    public void onStop(){
        super.onStop();
        mediaPlayer.release();
        mediaPlayer = null;
    }

}
