﻿# Kruger Park App
## Membres
Kleyton do Amparo
Àlex Moreno
## Codi GitLab
[Codi a GitLab](https://gitlab.com/alexandermorenoitb/kruegerparkapp)
## Codi Trello
[Trello](https://trello.com/b/xmutD2iZ/krugerparkapp)
## Ítems desenvolupats 
Realitzar imatges amb Intents.
Guardar imatges al núvol amb l'eina FireBase.
Mostrar ubicacions amb Location.
## Funcionalitats extra  desenvolupades
Un joc d'esbrinar els animals amb MP3Ready.
Cambiar el tipus de mapa amb setMapType.